# 部署手册

### 基础软件安装
MySQL (5.5+):必选  
JDK (1.8.0_xxx):必选  
阿里开源项目DataX:如使用DataX原生程序，必须安装。


### 数据库初始化

[数据库脚本](https://gitee.com/fsxgt/dg-datagrid-datax/blob/master/dg-datagrid-install/db/init.sql)


### 系统安装
#### 方法一、直接下载datagrid安装包：[下载地址](https://pan.baidu.com/s/1hFMPgku_O8E0ZPh5oi9mtg?pwd=ABCD) 提取码:ABCD

unzip datagrid-1.0.1.zip

修改配置文件config/console.properties

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/console_config.png)


修改配置文件config/executor.properties
![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/executor_config.png)

dataXHomePath：为datax的安装地址  
console.address：为总控console的服务地址  

启动程序  
cd datagrid-1.0.1/bin  
chmod 777 *.sh  
./startConsole.sh  
./startExecutor.sh  
executor支持分布式部署



#### 方法二、编译打包（官方提供的tar包跳过）
从Git上面获得源代码，在根目录下执行  
mvn clean install  
执行成功后将会在工程的dg-datagrid-install目录下生成安装包




