# 用户使用手册

数据集成开发平台共分为五个部分，包括：数据源管理、数据开发、数据集成、任务调度、日志查询。

### 数据源管理

在数据源管理中可以配置多种数据源。点击数据源管理，点击新增按钮，在编辑弹窗中填写数据源配置信息，点击测试连接按钮可以测试数据库连接情况，若连接成功，点击保存按钮数据源即配置成功。下图以MySQL数据库为例。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_shujuyuanguanli.png)

### 数据开发

#### 添加流程

在数据开发中可以进行流程编排，点击新建按钮，可以进行新建目录、新建流程、新建主题的操作；点击修改或删除按钮可以对主题、流程、目录进行修改和删除操作；点击切换按钮可以切换主题。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_xinjianliucheng.png)

#### 离线同步组件

1.添加组件

拖拽添加离线同步组件，双击组件进行编辑，自定义步骤名称，选择同步工具。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/ues_tianjialixianzujian.jpg)

2.选择数据源

对于数据来源，左侧进行数据来源的选择，此处选择在数据源管理中配置好的数据源，数据表可以通过选择表或填写SQL的方式进行选择，数据ETL需要填写增量表达式以及增量起始值，填写完成后，点击获取字段按钮，字段名会自动出现在字段映射中。对于数据去向，右侧进行数据去向的选择，也可以添加导入前SQL和导入后SQL。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_xuanzeshujuyuan.png)

3.字段映射

点击获取字段按钮，字段名自动出现在字段映射中，可以通过点击同名映射或同行映射自动连线进行映射，也可以通过拉取箭头进行手动映射。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_ziduanyingshe.png)

4.通道控制

通道控制默认配置如下，可以修改配置内容以匹配不同的需求，点击确定保存组件内容。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_tongdaokongzhi.png)

#### 抓取数据（单笔）组件

首先，拖拽添加抓取数据（单笔）组件，双击组件进行编辑，自定义步骤名称，选择执行SQL的数据源，在编辑弹窗中填写SQL语句，点击获取字段按钮，再点击添加按钮以添加SQL的筛选条件，选择字段名和取值字段，确保该字段有且只有单笔数据，最后自定义输入变量名，案例变量名为timeID4G，该变量名作为后续调用的变量名称，最后点击确定按钮进行保存。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_huoqudanbi.png)

#### 抓取数据（集合）组件

首先，拖拽添加抓取数据（集合）组件。双击组件进行编辑，自定义步骤名称，选择执行SQL的数据源，在弹窗中填写SQL语句，点击获取字段按钮，再点击添加按钮以添加sql的筛选条件，选择字段名和取值字段。以采集的电表点位数据SQL为例，将SQL中相同type的不同eid作为一个集合变量。首先，点击添加按钮，筛选条件选择type=电流，取值字段选择eid，变量名自定义为paList，同理，将SQL中type字段为正向有功的eid作为一个集合，变量名定义为pjList，最后点击确定按钮进行保存。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_zhuaqujihezujian.png)

#### 执行SQL组件

首先，拖拽添加执行SQL组件，双击组件进行编辑，自定义步骤名称，选择数据源。选择是否开启事务、是否使用mybatis语义、默认为否，填写延迟执行SQL的时间，默认为空。填写sql文本，在SQL中使用变量，使用格式为：**#{变量名}**

系统内置变量：

```
当前时间     curDateTime 
当前小时     curHour
当前分钟     curMinute
当前半分钟   curMinute30
当前日       curDay
```

例如：

使用单笔变量或系统内置变量：#{timeID4G}

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_zhixingsql.png)

使用集合变量： eid in (#{paList})

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_zhixingsql2.png)

#### 接口调用组件

首先，拖拽添加http接口调用组件，双击组件进行编辑，自定义步骤名称，编辑请求地址、请求类型、请求参数、断言值、断言路径、编码格式，最后点击确定按钮进行保存。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/usez_jiekoudiaoyong.png)

#### 条件分支组件

首先，拖拽添加条件分支组件，双击组件进行编辑，点击增加按钮，自定义步骤名称，在筛选条件中填写变量名，选择关系，编辑值，选择目标节点，即可实现当变量符合筛选条件则跳转执行目标节点，最后，点击确定按钮进行保存。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_tiaojianfenzhi.png)

#### 组件流程编排

拖拽组件进行流程编排，箭头的指示方向为流程的执行顺序。以电表数据流程为例。当多个箭头指向同一个组件时，则需箭头起始端组件全部执行结束，才可执行箭头结束端组件。组件流程编排结束后，点击保存按钮保存流程、点击运行按钮调试流程、点击查看日志按钮进行调试或排错，选择组件后点击停用按钮，该组件会被停用不参与执行，点击启用按钮，组件恢复可执行状态。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_liuchengbianpai.png)

### 数据集成

数据集成使用数据开发中的离线同步组件，详细操作参考离线同步组件。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_shujujucheng.png)

### 任务调度

在任务调度中可以进行任务定时设置，点击新建按钮，在编辑弹窗中自定义任务名称，选择任务类型，在业务流程中选择对应任务，选择任务触发类型，编辑并发数和超时时间，若设置任务流程为定时任务，则需要编辑cron表达式为任务进行定时；若设置任务为依赖任务，则需要配置上级任务，即当上级任务执行结束则执行该任务，最后，点击确定进行保存。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_renwudiaodu.png)

点击任务调度可以直接查看任务信息，新配置好的任务需要将状态改为已开启，在操作栏中可以对任务进行操作，包括：编辑任务、停止任务、执行任务、删除任务、查看任务日志。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_renwudiaodu2.png)

### 日志查询

在日志查询中可以查看任务日志，可以通过筛选栏对任务时间、任务名称、执行结果进行条件查询，筛选得到对应任务，点击操作栏的日志查询按钮可以查看任务的各组件详细执行信息。

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/use_rizhichaxun.png)