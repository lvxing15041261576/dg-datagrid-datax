#!/bin/bash
#

MAIN_JAR=dg-datagrid-executor*.jar

abs_path(){
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "${SOURCE}" ]; do
        DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
        SOURCE="$(readlink "${SOURCE}")"
        [[ ${SOURCE} != /* ]] && SOURCE="${DIR}/${SOURCE}"
    done
    echo "$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
}


BIN=`abs_path`

MAIN_JAR=$(find ${BIN}/../ -name ${MAIN_JAR})

if [ "x${JAVA_HOME}" != "x" ]; then
  JPS=${JAVA_HOME}/bin/jps
else
  JPS="jps"
fi



p=`${JPS} -l | grep "${MAIN_JAR}" | awk '{print $1}'`



case "`uname`" in
        CYCGWIN*) taskkill /PID "${p}" ;;
        *) kill -SIGTERM "${p}" ;;
esac










