#!/bin/bash
#


abs_path(){
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "${SOURCE}" ]; do
        DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
        SOURCE="$(readlink "${SOURCE}")"
        [[ ${SOURCE} != /* ]] && SOURCE="${DIR}/${SOURCE}"
    done
    echo "$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
}


BIN=`abs_path`

MAIN_JAR=dg-datagrid-console*.jar
SHELL_LOG="${BIN}/console.out"



MAIN_JAR=$(find ${BIN}/../lib -name ${MAIN_JAR})



JAVA_OPTS=" -jar "


if [ "x${JAVA_HOME}" != "x" ]; then
  EXE_JAVA=${JAVA_HOME}"/bin/java "${JAVA_OPTS}" "${MAIN_JAR}" --spring.config.location="${BIN}"/../config/console.properties"  
  JPS=${JAVA_HOME}/bin/jps
else
  EXE_JAVA="java "${JAVA_OPTS}" "${MAIN_JAR}" --spring.config.location="${BIN}"/../config/console.properties"  
  JPS="jps"
fi




echo "start log in ${SHELL_LOG}"

nohup ${EXE_JAVA} >${SHELL_LOG} 2>&1 &






