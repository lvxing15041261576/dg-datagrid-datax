# DataGrid数据集成开发平台

DataGrid是基于DataX的可视化的分布式数据集成开发工具，提供简单易用的流程编排页面，任务配置页面，数据同步页面，降低Datax的使用成本。  
DataGrid作为抚顺新钢铁下辖研究院的开源项目，已经在本企业的数字化进程中深度使用。  
欢迎企业或个人联合开发。


# Architecture diagram

![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/jiagou.png)


# System Requirements

- Language: Java 8（jdk版本建议1.8.201以上）
- Environment: MacOS, Windows,Linux
- Database: Mysql

# Features
- 1.图形化拖拽式流程编排，使用更简单
- 2.流程编排时可以自由启用或停用任意节点，并实时显示运行状态，让开发过程更简单
- 3.支持mybatis语义解析，可以直接使用mybatis编写sql,再结合变量配置机制，让sqL开发更加灵活
- 4.简单点击即可完成数据抽取作业，同时支持原生datax调用方式，让历史datax作业迁移更容易
- 5.支持动态修改任务状态、启动/停止任务，以及终止运行中任务，即时生效。
- 6.任务支持多层级依赖关系，当父任务执行结束后自动触发子任务。
- 7.支持多主题，多用户及权限控制
- 8.支持Mysql,Oracle,Postgresql,Sqlserver、Hive、HBase、ClickHouse、MongoDB等数据源页面化配置
- 9.数据库用户名密码自动加密，保证使用安全
- 10.支持任务超时，任务运行超时将会主动中断任务。
- 11.支持任务重试，当任务失败时将会按照预设的失败重试次数重试出错节点。
- 12.调度采用console-executor设计，支持集群部署
- 13.自研流程引擎，内存中运行，支持高并发调度，保证执行效率。
- 14.仅依赖mysql数据库，不需要额外搭建任何其他服务，安装过程十分容易。


# Quick Start


[部署手册 ](https://gitee.com/fsxgt/dg-datagrid-datax/blob/master/dg-datagrid-doc/deploy.md) 

[用户手册](https://gitee.com/fsxgt/dg-datagrid-datax/blob/master/dg-datagrid-doc/use.md)

# Introduction

### 1.数据源管理
支持Mysql,Oracle,Postgresql,Sqlserver、Hive、HBase、ClickHouse、MongoDB等数据源
![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/shujuyuan.png)

### 2.数据集成
用户通过简单几次点击，即可完成数据同步配置。支持直接选择表或编写SQL进行同步。  
同步工具支持三种模式  
datax-skin:系统会用户页面上配置自动生成datax配置，然后使用原生datax执行，需要搭建datax环境，配置datax_home系统参数。  
![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/shujujicheng1.png)
![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/shujujicheng2.png)
datax-fast:前端页面与datax-skin相同，基于datax源码，扩展开发的数据同步模块。excuter直接使用方法调用，省去datax初始化JVM的过程，所以启动速度提高10倍以上，适用于分钟级乃至秒级的数据同步。  
datax-shell:原生datax，shell调用，用户需要自主编辑同步配置文件  
![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/shujujicheng3.png)

### 3.数据开发
可视化数据处理流程开发。支持串行、并行、事务控制，条件分支、多流合并。  
支持局部运行，代码级运行日志输出，支持多主题切换。
![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/shujukaifa.png)

### 4.任务调度
支持任务定时,多层级调度,任务运行资源控制。  
基于微服务的console-executor分布式架构，支撑高并发调度，理论上并发数没有上限。  
阻塞处理策略:当单个任务没有运行完成时，又再次启动，会丢弃新启动的调度
![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/renwudiaodu.png)

### 5.用户管理
角色简单分为超级用户可普通用户，超级用户可以编辑用户信息并具有管理所有用户数据的权限
![](https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/yonghuguanli.png)


# UI

[前端源码](https://gitee.com/fsxgt/dg-datagrid-ui)


# 项目成员
- 六  
负责整体架构和后端开发

- Ripper  
参与了前期技术验证和技术选型

- 阿哲  
前端架构

- 坚持 努力 听话  
负责前端开发

- 吐个泡泡  
测试人员，也是本系统的主要使用者

- 阿轩  
测试人员，也是本系统的主要使用者


感谢贡献！

# Contributing

Contributions are welcome! Open a pull request to fix a bug, or open an Issue to discuss a new feature or change.

欢迎参与项目贡献！比如提交PR修复一个bug，或者新建 Issue 讨论新特性或者变更。

# Copyright and License

Apache-2.0 license

产品开源免费，并且将持续提供免费的社区技术支持。个人或企业内部可自由的接入和使用。


# Contact us

### 个人微信
<img src="https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/weixin.jpg" width="115px" height="115px">


### QQ交流群
<img src="https://gitee.com/fsxgt/dg-datagrid-datax/raw/master/dg-datagrid-doc/imgs/qq.jpg" width="115px" height="115px">
